(define-module (chop eval stats))

;;; Author: Ludovic Court�s
;;;
;;; Commentary:
;;;
;;; Provide simple mechanisms to manipulate overall file chopping
;;; statistics.
;;;
;;; Code:

(use-modules (srfi srfi-9)  ;; records
	     (srfi srfi-17) ;; generalized `set!'
	     (chop eval records)  ;; tools
	     )

(define-public-simple-record-type chop-stats
  (input-bytes data-bytes-written data-virgin-bytes
   meta-bytes-written meta-virgin-bytes))


(define-public (make-chop-stats input-bytes
				data-bytes-written data-virgin-bytes
				meta-bytes-written meta-virgin-bytes)
  (%make-chop-stats input-bytes
		    data-bytes-written data-virgin-bytes
		    meta-bytes-written meta-virgin-bytes))

(define-public (make-chop-stats/zero)
  "Return a zero-initialized @code{chop-stats} object."
  (%make-chop-stats 0 0 0 0 0))

(define-macro (add-up! total stats field)
  (let ((accessor (symbol-append 'chop-stats: field)))
    `(set! (,accessor ,total)
	   (+ (,accessor ,total) (,accessor ,stats)))))

(define-public (add-chop-stats! total entity)
  "Modify the @code{chop-stats} object @var{total} by adding @var{entity} to
it.  Return @var{total}."
  (add-up! total entity input-bytes)
  (add-up! total entity data-bytes-written)
  (add-up! total entity data-virgin-bytes)
  (add-up! total entity meta-bytes-written)
  (add-up! total entity meta-virgin-bytes)
  total)

(define-public (chop-stats:virgin-bytes stats)
  "Return the actual size occupied by the data and meta-data, according to
@var{stats}."
  (+ (chop-stats:data-virgin-bytes stats)
     (chop-stats:meta-virgin-bytes stats)))

(define-public (chop-stats:virgin-bytes/input-bytes stats)
  "Return the ratio virgin-bytes over input-bytes."
  (exact->inexact (/ (chop-stats:virgin-bytes stats)
		     (chop-stats:input-bytes stats))))

(define-public (chop-stats:meta-virgin-bytes/input-bytes stats)
  "Return the ratio..."
  (exact->inexact (/ (chop-stats:meta-virgin-bytes stats)
		     (chop-stats:input-bytes stats))))


;;; arch-tag: ff787b60-e3fe-45fc-9168-f8b529b7ef92

;;; stats.scm ends here
