#!/bin/sh
# aside from this initial boilerplate, this is actually -*- scheme -*- code
main='(module-ref (resolve-module '\''(chop-eval)) '\'main')'
exec ${GUILE-guile} --debug -l $0 -c "(apply $main (cdr (command-line)))" "$@"
!#
;;; chop-eval --- Does something

;; 	Copyright (C) 2002 Free Software Foundation, Inc.
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this software; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
;; Boston, MA 02111-1307 USA

;;; Author:  Ludovic Court�s <ludovic.courtes@laas.fr>

;;; Commentary:

;; Usage: chop-eval [ARGS]
;;
;; chop-eval does something.
;;
;; TODO: Write it!

;;; Code:

(define-module (chop-eval)
  #:export (chop-eval))

(use-modules (chop streams)
	     (chop stores)
	     (chop choppers)
	     (chop indexers)
	     (chop store-stats)

	     (chop eval stats)

	     (srfi srfi-1))


;;;
;;; Utilities.
;;;

(define (file-size file)
  (if (file-exists? file)
      (stat:size (stat file))
      #f))

(define (% thing ref)
  (exact->inexact (* 100 (/ thing ref))))


;;;
;;; Indexing files.
;;;

(define (make-default-chopper stream)
  (chopper-generic-open "anchor_based" stream 8192))

(define (make-default-indexer)
  (hash-tree-indexer-open 'hash-method/sha1 'hash-method/sha1 #f))

(define (index-whole-file file store-name)
  (let* ((stream (file-stream-open file))
	 (real-store (file-based-block-store-open "tdb" store-name))
	 (data-store (stat-block-store-open "data" real-store #f))
	 (meta-data-store (stat-block-store-open "meta-data"
						 real-store #f))
	 (chopper (make-default-chopper stream))
	 (indexer (make-default-indexer)))
    (indexer-index-blocks indexer chopper
			  data-store meta-data-store)
    (let ((ret (cons (stat-block-store-stats data-store)
		     (stat-block-store-stats meta-data-store))))
      (for-each store-close
		(list data-store meta-data-store real-store))
      ret)))

(define *scratch-store* ",,file-blocks.db")


;;;
;;; The program.
;;;

(define (chop-eval . files)
  (if (file-exists? *scratch-store*)
      (delete-file *scratch-store*))
  (let ((res
	 (fold (lambda (file result)
		 (format #t "~%* indexing `~a'...~%" file)
		 (let* ((stats (index-whole-file file *scratch-store*))
			(data (block-store-stats:bytes-written (car stats)))
			(meta (block-store-stats:bytes-written (cdr stats)))
			(data- (block-store-stats:virgin-bytes (car stats)))
			(meta- (block-store-stats:virgin-bytes (cdr stats)))
			(orig-size (file-size file)))
		   (format #t "  originial size: ~a bytes~%" orig-size)
		   (format #t "  data: ~a bytes~%" data)
		   (format #t "  meta: ~a bytes~%" meta)
		   (format #t "  ratio: ~a%~%" (% meta data))
		   (format #t "  virgin: ~a bytes~%"
			   (+ data- meta-))

		   (add-chop-stats! result
				    (make-chop-stats orig-size
						     data data-
						     meta meta-))))
	       (make-chop-stats/zero)
	       files)))

    (format #t "~%* final result~%")
    (format #t "  input size: ~a bytes~%"
	    (chop-stats:input-bytes res))
    (format #t "  actual data size: ~a bytes [~a% of the input]~%"
	    (chop-stats:virgin-bytes res)
	    (chop-stats:virgin-bytes/input-bytes res))
    (format #t "  actual meta size: ~a bytes [~a% of the input]~%"
	    (chop-stats:meta-virgin-bytes res)
	    (chop-stats:meta-virgin-bytes/input-bytes res))))

(define main chop-eval)

;;; arch-tag: 4ec46318-2dbb-4c59-b630-3cd4c1616be1

;;; chop-eval ends here
